package com.example.homework01;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Fragment_connect extends Fragment {

    //private String context;//用于获取textview的值
    //private TextView mTextView;//用于获取textview控件


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_connect, container, false);
        //mTextView = (TextView) view.findViewById(R.id.txt_connect);
        //mTextView.setText(context);
        return view;
        // return inflater.inflate(R.layout.fragment_connect, container, false);
    }
}